#programa que aceita uma sequencia de numeros separados por virgula como argumento e gera
#uma lista e uma tupla com elementos.
def list(numbers):
    numbers_list = numbers.split(',')
    tup_of_list = tuple(numbers_list)
    return(numbers_list, tup_of_list)

#programa que aceita um texto como entrada(stdin) e tranforma o texto em todo maiusculo
import sys
print sys.stdin.read().upper()
