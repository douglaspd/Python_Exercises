from os import system
from os import sys

def tela():
     print "\n ---------------------------------------------- "
     print "       *  Calculadora em Python"
     print "\n       -------------------------------      "

tela()

def sum():
    first_value = input("Please insert the first number: ")
    second_value = input("Please insert the second number: ")

    x = float(first_value)
    y = float(second_value)

    final_sum = float(x + y)
    print ("Resultado: %.8f + %.8f = %.8f ") %(x, y, final_sum)

    options()

def subtract():
    first_value = input("Please insert the first number: ")
    second_value = input("Please insert the second number: ")

    x = float(first_value)
    y = float(second_value)

    final_subtract = float(x - y)
    print ("Resultado: %.8f - %.8f = %.8f ") %(x, y, final_subtract)

    options()

def multiply():
    first_value = input("Please insert the first number: ")
    second_value = input("Please insert the second number: ")

    x = float(first_value)
    y = float(second_value)

    final_multiply = float(x * y)
    print ("Resultado: %.8f * %.8f = %.8f ") %(x, y, final_multiply)

    options()

def divide():
    first_value = input("Please insert the first number: ")
    second_value = input("Please insert the second number: ")

    x = float(first_value)
    y = float(second_value)

    final_divide = float(x / y)
    print ("Resultado: %.8f / %.8f = %.8f ") %(x, y, final_divide)

    options()

def options():
    try:
     option = input("Options: \n      (1) Sum \n      (2) Subtract \n      (3) Multiply \n      (4) Divide \n      (5) Sair \n")
    except:
     print ("\n This option doesn't exist. \n")
     options()

    if (  option < 1 or option > 5):
     print ("Sorry, this option doesn't exist. \n")
     options()

    elif ( option == 1 ):
     sum()

    elif ( option == 2 ):
     subtract()

    elif ( option == 3 ):
     multiply()

    elif ( option == 4 ):
     divide()

    elif ( option == 5 ):
     sys.exit()

options()
